
from bioblend.galaxy import GalaxyInstance

# Connect to Galaxy instance
galaxy_url = 'https://galaxy.pasteur.fr'
api_key = '31f05d9edaa2228b66c538f43b0d5d52'
gi = GalaxyInstance(url=galaxy_url, key=api_key)

# List all histories
listhist = gi.histories.get_histories()
print(listhist)
for h in listhist:
    if h['name'] == 'shaman_1105_84':
        for dataid in gi.histories.show_history(h['id'])['state_ids']['ok']:
            print("purge {}".format(dataid))
            gi.histories.delete_dataset(h['id'], dataid, purge=True)
        gi.histories.delete_history(h['id'], purge=True)
